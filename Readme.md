## README ##

### Prerequisites ###

* SSH Keys
    * This assumes ssh keys have been setup for passwordless. My recommended setup guide is [here](https://www.cyberciti.biz/faq/how-to-set-up-ssh-keys-on-linux-unix/) if you have not done it.
* CRON Job
    * If you want this to be automatically run which is kinda the point there is an example cron job that runs every 5 mins. Copy the file to /etc/cron.d/ and update the pathing if you install it to a different location to below
* Sudoers File
    * I have included an example sudoers file that needs to be setup on each pi to be able to run pihole commands without passwords and the file copies. 

#### How to get started ####

Assuming all prerequisites have been fulfilled 

Clone the repo

	$ sudo git clone https://bhutchin@bitbucket.org/bhutchin/pync.git
	Cloning into 'pync'...

Update the config file to sync the files you wish to keep synced. The default config includes the whitelist.txt, blacklist.txt and adlists.list

The system config section is required to specify the user to run the copies as so this does need to be the user configured in both the authorized keys section and the sudoers file. The hosts is just a comma delimited list of all hosts that can be as many as required.


	$ cd pync
	$ cat config/tync.ini
	[SYSTEM]
	hosts = pi01,pi02
	user =  pi

	[WHITELIST]
	file = whitelist.txt
	remote_dir = /etc/pihole/

	[ADLIST]
	file = adlists.list
	remote_dir = /etc/pihole/

	[BLACKLIST]
	file = blacklist.txt
	remote_dir = /etc/pihole/

Once the config has been setup, enter the directory and run the pihole tync file
	
	$ cd pync
    $ python3 scripts/pync.py
	
This will run the script against all configurations setup in the config file		
	

That should be it, back up your files beforehand I do not want to hear the this broke something but do feel free to let me know if there are improvements you would like to see.

