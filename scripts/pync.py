import subprocess
import tync

CONFIG_FILE = '/opt/pync/config/tync.ini'

def get_hosts_data():
    CONFIG_DATA, USER, HOSTS = tync.read_config(CONFIG_FILE)
    HOSTS = HOSTS.split(',')
    return HOSTS, USER


def update_pihole():
    hosts, user = get_hosts_data()
    for host in hosts:
        args = [
            'ssh',
            user + '@' + host,
            '/usr/bin/nohup',
            'sudo',
            '/usr/local/bin/pihole',
            '-g',
            '&'
        ]
        print('Updating pihole for ' + host)
        subprocess.Popen(args)


def run_tync_sync():
    if tync.run_sync():
        update_pihole()

if __name__ == "__main__":
    run_tync_sync()