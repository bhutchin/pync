import configparser
import filecmp
import os
import subprocess
import time

CONFIG_FILE = '/opt/pync/config/tync.ini'

def read_config(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    config_data = []
    for section in config.sections():
        if section != 'SYSTEM':
            config_dict = {}
            config_dict.update({'section': section})
            for item in config[section].items():
                config_dict.update({item[0]: item[1]})
            config_data.append(config_dict)
        else:
            user = config[section]['user']
            hosts = config[section]['hosts']
    return config_data, user, hosts

def update_file(hosts, user, directory, remote_file):
    files = get_remote_file(hosts, user, directory, remote_file)
    if not os.path.isfile(files[0]):
        time.sleep(1)
    if check_diff(files):
        print("Files differ updating: " + directory + remote_file)
        comb_list = create_combined_list(files)
        local_file = write_out_new_list(comb_list, remote_file)
        copy_to_remote(user, hosts, local_file, directory, remote_file)
        return True
    for temp_file in files:
        os.remove(temp_file)


def get_remote_file(hosts, user, path, file_name):
    location = path + file_name
    files = []
    for host in hosts:
        local_file = '/tmp/' + host + '_' + file_name
        args = [
            '/usr/bin/scp',
            user + '@' + host + ':' + location,
            local_file
        ]
        subprocess.Popen(args)
        files.append(local_file)
    return files

def check_diff(files):
    if len(files) == 2:
        if not filecmp.cmp(files[0], files[1]):
            return True
    else:
        match = True
        for data in files:
            if not filecmp.cmp(data, files[0]):
                match = False
                break
        if not match:
            return True

def create_combined_list(local_lists):
    combined_list = []
    for lists in local_lists:
        with open(lists) as open_file:
            list_entries = open_file.readlines()
            combined_list = combined_list + list_entries
    cleaned_list = set(combined_list)
    return cleaned_list

def write_out_new_list(list_to_write, temp_file_name):
    temp_file = '/tmp/' + temp_file_name
    temp_file_data = open(temp_file, "w")
    for entry in list_to_write:
        temp_file_data.write(entry)
    temp_file_data.close()
    return temp_file

def copy_to_remote(user, hosts, local_file, remote_dir, remote_file):
    remote_location = remote_dir + remote_file
    for host in hosts:
        args = [
            '/usr/bin/scp',
            local_file,
            user + '@' + host + ':' + remote_location
        ]
        print(args)
        subprocess.Popen(args)
    os.remove(local_file)

def run_sync():
    CONFIG_DATA, USER, HOSTS = read_config(CONFIG_FILE)
    HOSTS = HOSTS.split(',')
    updated = False
    for entry in CONFIG_DATA:
        print('Attempting: ' + entry['remote_dir'] + entry['file'])
        updated = update_file(HOSTS, USER, entry['remote_dir'], entry['file'])

    if updated:
        return True

if __name__ == "__main__":
    run_sync()
